#include <iostream>

using namespace std;

///Maximul dintre 2 numere

int main()
{
    int a, b; ///declaram a si b (numere naturale)

    cout << "Introduceti 2 numere: ";
    cin >> a >> b; ///citim a si b

    if(a > b) ///dacă a e mai mare ca b
    {
        cout << "maximul este: " << a; ///atunci maximul este a (afișez a)
    }
    else
    {
        cout << "maximul este: " << b; ///maximul este b (afișez b)
    }

    return 0;
}
