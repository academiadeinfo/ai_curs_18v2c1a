#include <iostream>

using namespace std;

int main()
{
    string pahar_a, pahar_b, pahar_aux;

    cout << "Pahar a: ";
    cin >> pahar_a;
    cout << "Pahar b: ";
    cin >> pahar_b;
    cout << endl << "--- interschimbare ---" << endl << endl;

    pahar_aux = pahar_a;
    pahar_a = pahar_b;
    pahar_b = pahar_aux;

    cout << "Pahar a: " << pahar_a << endl;
    cout << "Pahar b: " << pahar_b << endl;

    return 0;
}
