#include <iostream>

using namespace std;

/**
Scrieți un program care citește Numele și vârsta a două persoane
și afișează numele persoanei cu vârsta mai mare
*/

int main()
{
    ///declarare
    string nume1, nume2;
    int varsta1, varsta2;

    ///citim datele de intrare (cele 2 nume și vârste)
    cout << "Introduceti 2 nume: ";
    cin >> nume1 >> nume2;
    cout << "Cati ani are " << nume1 <<"? ";
    cin >> varsta1;
    cout << "Cati ani are " << nume2 <<"? ";
    cin >> varsta2;

    if(varsta1 == varsta2)///( * ) dacă vârsta1 este egală cu vârsta2
    {
        cout << nume1 << " si " << nume2 << " au aceeasi varsta." <<endl;///afișăm mesaj corspunzător
    }
    else
    {
        if(varsta1 > varsta2)///dacă vârsta1 este mai mare decat varsta2
        {
            cout << nume1 << " este mai in varsta" << endl;///afișăm nume1
        }
        else///altfel
        {
            cout << nume2 << " este mai in varsta" << endl;///afișăm nume2
        }
    }

    return 0;
}
