#include <iostream>

using namespace std;

int main()
{
    int nr = 3; /// nr este la îceput un numă diferit de 0.
    int dreapta, stanga; ///dreapta retine nr pare, stanga retine nr impare
    dreapta = 0;
    stanga = 0;

    while(nr != 0)
    {
        cin >> nr;
        if(nr % 2 == 0) ///dacă nr este par
        {
            cout << nr << " este par" << endl;
            dreapta = dreapta + 1;
        }
        else
        {
            cout << nr << " este impar" << endl;
            stanga = stanga + 1;
        }
    }

    cout << endl;
    cout << "pare: " << dreapta <<endl;
    cout << "impare: " << stanga << endl;

    return 0;
}
