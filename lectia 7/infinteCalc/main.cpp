#include <iostream>

using namespace std;

int main()
{
    string continua = "da";

    while(continua == "da")
    {
        int a, b;
        char semn;
        cin >> a >> semn >> b;
        if(semn == '+')
            cout << " = " << a + b;
        else if(semn == '-')
            cout << " = " << a - b;

        cout << endl << "Vrei sa continui? (da / nu): " << endl;
        cin >> continua;
        cout << endl;
    }

    return 0;
}
