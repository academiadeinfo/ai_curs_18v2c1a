#include <iostream>

using namespace std;

///ultima cifră a unui nr n:  n % 10
///n fără ultima cifră:  n / 10

int main()
{
    int n;
    cout << "n = ";
    cin >> n;

    int faraUltimaCifra = n / 10; //initializare la declarare.
    int penultima = faraUltimaCifra % 10; // ultima cifră a numărului fară ultima cifră

    if(penultima > 5)
        cout << "Penultima cifră este mai mare decât 5" << endl;
    else
        cout << "Penultima cifră nu este mai mare decât 5" << endl;


    return 0;
}
