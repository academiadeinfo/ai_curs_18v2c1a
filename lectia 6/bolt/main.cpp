#include <iostream>

using namespace std;

int main()
{
    int Elsa, Elif;///declrăm variabile
    cout << "Elsa: ";
    cin >> Elsa;///citim ultimul număr spus de Elsa

    if(Elsa % 7 == 0 or Elsa % 10 == 7)///dacă număul spus de Elsa era greșit (era divizibil cu 7 sau avea ucif 7)
        cout << "Elif: Yey, am castigat (^_^)"<<endl; ///atunci afișăm "am castigat"
    else///altfel
    {
        Elif = Elsa + 1; ///calculăm următorul număr
        if(Elif % 7 == 0 || Elif % 10 == 7)///dacă următorul număr respecta regula
            cout << "Elif: BOLT! (-_0)" <<endl; ///afișăm BOLT
        else //altfel
            cout << "Elif: " << Elif <<endl;///afișăm următorul număr
    }

    return 0;
}
