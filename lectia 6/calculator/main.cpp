#include <iostream>

using namespace std;

int main()
{
    int a, b; ///declar 2 variabile întregi
    char op; ///operator

    cin >> a >> op >> b;

    if(op == '+')
        cout << " = " << a + b << endl;
    else if(op == '-')
        cout << " = " << a - b << endl;
    else if(op == '*')
        cout << " = " << a * b <<endl;
    else if(op == '/' or op == '%')
    {
        cout << "cat  = " << a / b << endl;
        cout << "rest = " << a % b << endl;
    }
    else ///dacă nu a fost niciuna din operatiile respective, se va afișa "operatie necunoscută"
    cout << "operatie necunoscuta" << endl;

    return 0;
}
