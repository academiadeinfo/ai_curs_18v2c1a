#include <iostream>

using namespace std;
/**
Scrieți un program care citește Numele, greutatea și înălțimea a două persoane.
Afișați numele persoanei mai grase.
O persoană este consideradă mai grasă dacă raportul greutate/înălțime este mai mare.
*/

int main()
{
    string nume1, nume2;
    double greu1, greu2, inalt1, inalt2;

    cout << "Persoana 1: "<< endl;
    cout << "Nume: ";
    cin >> nume1;
    cout << "Greutate: ";
    cin >> greu1;
    cout << "Inaltime: ";
    cin >> inalt1;

    cout << "Persoana 2: "<< endl;
    cout << "Nume: ";
    cin >> nume2;
    cout << "Greutate: ";
    cin >> greu2;
    cout << "Inaltime: ";
    cin >> inalt2;

    ///PROVOCARE: dacă una din persoane are numele vostru, programul să spună că cealaltă
    ///           persoană e mai grasă.
    cout << endl;
    if(nume1 == "Elif" or nume2 == "Elif")
        cout << "Elif este mai slab";
    else
    {
        if(greu1 / inalt1  >  greu2 / inalt2)
            cout << nume1 << " este mai gras / grasa.";
        if(greu1 / inalt1  <  greu2 / inalt2)
            cout << nume2 << " este mai gras / grasa.";
        if(greu1 / inalt1  ==  greu2 / inalt2)
            cout << nume1 << " si " << nume2 << " sunt la fel de grasi / grase";
    }

    return 0;
}
