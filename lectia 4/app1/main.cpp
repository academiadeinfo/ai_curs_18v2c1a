#include <iostream>

using namespace std;

/**
Se citește un cuvant reprezentând numele eleveului x.
Apoi se citesc 3 numere, reprezentând notele elevului x.
Să se afișeze media lui x și dacă a promovat sau nu anul.
*/

int main()
{
    string nume;
    int nota1, nota2, nota3;
    double media;

    cout << "Nume elev: ";
    cin >> nume;
    cout << "Introduceti cele 3 note: ";
    cin >> nota1 >> nota2 >> nota3;

    media = (nota1 + nota2 + nota3) / 3.0;

    cout << "media: " << media << endl;

    if( media > 5)///dacă media este mai mare decât 5
        cout << "Felicitari " << nume << "! Ai promovat" << endl;//eleveul a promovat
    else ///altfel
    {
        cout << nume << ", Ne vedem la toamna!" <<endl; ///a ramas corigent
    }


    return 0;
}
