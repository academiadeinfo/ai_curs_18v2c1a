#include <iostream>

using namespace std;

/**
    Elsa vrea să cumpere mere.
    Un kg de mere costă c codoni, iar Elsa are exact a codoni.
    Numerele c și a se vor citi de la tastatură.
    Dacă Elsa poate cumpăra cel puţin un kg de mere să se afişeze
    mesajul “DA” și numărul maxim de kg pe care le poate cumpăra.
    În cazul în care Elsa nu poate cumpăra niciun kg de mere să se afișeze  “NU”.

*/

int main()
{
    int a, c;

    cout << "Cati codoni are Elsa";
    cin >> a;

    cout << "Cat costa un kilogram de mere";
    cin >> c;

    if(a > c)
    {
        cout << "poate cumpara maxim: " << a / c;

    }
    else
    {
        cout << "NU poate cumpara nici un kilogram de mere";
    }

    return 0;
}
