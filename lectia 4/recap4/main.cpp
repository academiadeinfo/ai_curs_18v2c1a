#include <iostream>

using namespace std;

int main()
{
    int Elif, Elsa;
    int x, a, b;

    cout << "Cate bomboane au primit de la batranul Switch? ";
    cin >> x;
    Elif = x;
    Elsa = x;

    cout << "Cate bomboane a pierdut Elif? ";
    cin >> a;
    Elif = Elif - a;

    cout << "Cate bomboane ii da Elsa lui Elif? ";
    cin >> b;
    Elsa = Elsa - b;
    Elif = Elif + b;

    cout << "Elsa mai are: " << Elsa << endl;
    cout << "Elif mai are: " << Elif << endl;

    return 0;
}
