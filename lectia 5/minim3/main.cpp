#include <iostream>

using namespace std;

/// Minimul dintre 3 numere;

int main()
{
    int a, b, c;
    cout << "Introduceti 3 numere: ";
    cin >> a >> b >> c;

    if(a < b) ///dacă a este mai mic decât b
    {
        ///minimul este dintre a și c
        if(a < c)
            cout << "Minimul este: " << a;
        else
            cout << "Minimul este: " << c;
    }
    else
    {
        ///minimul este dintre b și c
        if(b < c)
            cout << "Minimul este: " << b;
        else
            cout << "Minimul este: " << c;
    }

    return 0;
}
