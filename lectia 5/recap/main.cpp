#include <iostream>

using namespace std;

/**
Scrieți un program care citește 2 numere naturale și
afișează minimul dintre ele.
*/

int main()
{
    int a, b;
    cout << "introduceti doua numere: ";
    cin >> a >> b;

    if(a < b)
        cout << "Minimul este: " << a;
    else
        cout << "Minimul este: " << b;

    return 0;
}
