#include <iostream>

using namespace std;

/**
Se citesc două numere naturale a și b.
să se compare cele două numere. Se va afișa:
    "a < b"     dacă a < b
    "a == b"   dacă a == b
    "a > b"     dacă a > b

*/

int main()
{
    int a, b;
    cout << "a = ";
    cin >> a;
    cout << "b = ";
    cin >> b;

    if(a < b)
        cout << "a < b";
    else if(b < a)
        cout << "a > b";
    else
        cout << "a == b";

    cout << endl;

    return 0;
}
