#include <iostream>

using namespace std;

int main()
{
    ///Afișăm produsele:
    cout << "Vrei sa inveti informatica?" << endl << endl;
    cout << "1. C++ Modulul 1: 350 codoni" << endl;
    cout << "2. Primii Pasi in IT: 350 codoni" << endl;
    cout << "3. Robotica Nivelul 1: 500 codoni" << endl;
    cout << "4. De la C++ la Python: 400 codoni" << endl;

    int cod; ///codul produsului dorit - trebuie citit
    int pret; ///pretul produsului dorit - trebuie setat în funcție de cod

    ///cerem si citim codul produsului dorit:
    cout << "Introduceti codul cursului dorit: ";
    cin >> cod;

    if(cod == 1)
    {
        cout << "Ati ales cursul C++ Modulul 1: 350 codoni";
        pret = 350;
    }
    else if(cod == 2)
    {
        cout << "Ati ales cursul Primii Pasi in IT: 350 codoni";
        pret = 350;
    }
    else if(cod == 3)
    {
        cout << "Ati ales cursul Robotica Nivelul 1: 500 codoni";
        pret = 500;
    }
    else if(cod == 4)
    {
        cout << "De la C++ la Python: 400 codoni";
        pret = 400;
    }

    ///partea 2: plata

    int bani; //cat bani a introdus utilizatorul
    cout << endl << endl << "Introduceti banii: ";
    cin >> bani;

    cout << endl;
    if(bani < pret)
        cout << "N-ai destui bani, mai incearca la toamna" << endl;
    else if(bani > pret)
    {
        cout << "Multumim. Academia de Info este pe str. Rozelor 48."<<endl;
        cout << "Aici este restul dvs: " << bani-pret << " codoni." << endl;
    }
    else
        cout << "Multumim. Academia de Info este pe str. Rozelor 48."<<endl;


    return 0;
}
