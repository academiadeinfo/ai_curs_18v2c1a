#include <iostream>

using namespace std;

/**
Scrieți un program care citește 3 numere raționale
și afișează media lor.
*/

int main()
{
    double a, b, c; ///declar 3 variabile raționale
    double media;

    cout << "Introduceti numere rationale: "; //cerem utilizatorului să introducă numerele
    cin >> a >> b >> c; // citim cele 3 numere

    media = (a + b + c) / 3;  //calculăm media

    cout << media;//afișăm media


    return 0;
}
