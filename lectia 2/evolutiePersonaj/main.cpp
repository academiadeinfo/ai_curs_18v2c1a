#include <iostream>

using namespace std;

int main()
{
    string nume, specie, trasatura;
    int varsta;
    double inaltime, greutate;
    double rataCrestere, rataIngrasare; ///cât crește / cât se îngrașă în fiecare an

    cout << "nume: ";
    cin >> nume;
    cout << "specie: ";
    cin >> specie;
    cout << "Cum este " << nume <<"? ";
    cin >> trasatura;
    cout << "Cati ani are? ";
    cin >> varsta;
    cout << "Ce inaltime are? ";
    cin >> inaltime;
    cout << "Ce greutate are? ";
    cin >> greutate;

    cout << endl << "Deci " << nume << " este un " << specie << " " << trasatura << " de " << varsta << " ani";
    cout << " si are " << inaltime << "m si " << greutate << "Kg." << endl;

    ///Citiți rataCreștere, rataIngrasare și numarul de ani care trec
    ///Afișați datele personajului după numărul respectiv de ani.

    cout << endl;
    cout << "Cat de mult creste " << nume << " in fiecare an? ";
    cin >> rataCrestere;
    cout << "Cat de mult se ingrasa " << nume <<" in fiecare an?";
    cin >> rataIngrasare;
    cout << endl << "cati ani au trecut? ";

    int ani;
    cin >> ani;

    varsta = varsta + ani;
    inaltime = inaltime + rataCrestere * ani;
    greutate = greutate + rataIngrasare * ani;

    cout << endl << "Acum " << nume << " este tot un " << specie << " " << trasatura;
    cout << " dar are " << varsta << " ani, "<< inaltime << "m si " << greutate << "Kg." << endl;



    return 0;
}
