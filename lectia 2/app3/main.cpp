#include <iostream>

using namespace std;

/**
Elsa are x bomboane. Ea mănâncă y bomboane.
Scrieți un program care citește din consolă numerele x și y
și afișează câte bomboane mai are Elsa.
*/

int main()
{
    int x, y;
    int rezultat;

    cout << "Cate bomboane are Elsa? ";
    cin >> x;

    cout << "Cate bomboane a mancat? ";
    cin >> y;

    rezultat = x - y;

    cout << "Mai are " << rezultat << " bomboane." << endl;


    return 0;
}
