#include <iostream>

using namespace std;

/**
Scrieți un program care citește 3 numere naturale
și afișează produsul lor.*
*/

int main()
{
    int numar1, numar2, numar3; //declarat 3 variabile

    cin >> numar1 >> numar2 >> numar3; //citim datele de intrare

    cout << "Produsul este: " << numar1*numar2*numar3; // afișăm rezultatul

    return 0;
}
