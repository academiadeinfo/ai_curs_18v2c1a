#include <iostream>

using namespace std;

int main()
{
    string nume, specie, culoare;
    double greutate;
    int varsta;

    nume = "Whiley";
    specie = "caine";
    culoare = "Alb";
    greutate = 3.2;
    varsta = 2;

    cout << nume << " este un " << specie << " " << culoare << endl;
    cout << "El are " << varsta << " ani si " << greutate << " kg." << endl;

    cout << endl << "Au trecut 2 ani si 1.3 Kg... " << endl << endl;

    varsta = varsta + 2;
    greutate = greutate + 1.3;

    cout << nume << " este un " << specie << " " << culoare << endl;
    cout << "El are " << varsta << " ani si " << greutate << " kg." << endl;

    return 0;
}
