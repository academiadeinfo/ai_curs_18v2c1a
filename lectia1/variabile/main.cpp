#include <iostream>

using namespace std;

int main()
{
    //Acesta este un comentariu
    /// Acesta este un comentariu îngroșat
    /**
        Acesta este un comentariu
        Pe mai multe linii;
    */

    int numar; //declar o variabilă de tipul int

    numar = 5; // salvez în variabila numar valoarea 5
    cout << numar << endl; // afișez conținutul variabilei numar (adică 5)

    numar = 7 + 3; // variabila număr primeste valoarea 7+3 adică 10
    cout << numar + 5 << endl; // afișez numar+5 adica 10+5 adica 15

    numar = numar - 2; // numar primeste cat era înainte numărul minus 2 adică 10-2 adică 8
    cout << numar << endl; // afișez numărul, adică 8;

    return 0;
}
